﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.VFX;

public class Death : MonoBehaviour
{
    public bool die;
    public bool respawn;
    public float timerDeath;
    public bool invincible;
    public float timerInvicibility;
    public float life;
    public bool addScore;
    public GameObject bike;
    public GameObject explosion;
    public GameObject trail;
    public Transform respawnPoint;
    public TrailCollision collisionScript;
    public CharacterController controller;
    public AudioSource deathSound;
    public TMP_Text timerRespawn;
    public int timerInt;

    public TMP_Text lifeText;
    // Start is called before the first frame update
    void Start()
    {
        explosion.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
     //   CapsuleCollider collider = GetComponent<CapsuleCollider>();
        PlayerMovement movement = GetComponent<PlayerMovement>();
      //  MeshRenderer mesh = GetComponent<MeshRenderer>();
        lifeText.text = life.ToString();
        
        
        if (die)
        {
            controller.enabled = false;
            movement.enabled = false;
            bike.SetActive(false);
            explosion.SetActive(true);
            timerInt = Mathf.FloorToInt(timerDeath);
            timerRespawn.enabled = true;
            timerRespawn.text = timerInt.ToString();
            if (timerDeath >= 0)
            {
                timerDeath -= Time.deltaTime;
            }
            if (timerDeath <= 0.05f)
            {
                addScore = true;
                trail.SetActive(false);
                collisionScript.enabled = false;
            }
            if (timerDeath <= 0)
            {
                die = false;
                timerInvicibility = 2;
                respawn = true;
                invincible = true;
                addScore = false;
                life--;
            }
        }

        if (die == false)
        {
            if (life >= 0 )
            {
                if (respawn)
                {
                    movement.enabled = true;
                    timerRespawn.enabled = false;
                    bike.SetActive(true);
                    controller.enabled = true;
                    if (timerInvicibility >= 0)
                    {
                        timerInvicibility -= Time.deltaTime;

                    }

                    if (timerInvicibility >= 1.9f )
                    {
                        transform.position = respawnPoint.position;
                        transform.rotation = respawnPoint.rotation;
                    }

                    if (timerInvicibility <= 0)
                    {
                        trail.SetActive(true);
                        explosion.SetActive(false);
                        collisionScript.enabled = true;
                        invincible = false;
                        respawn = false;
                    }
                }
            }
            timerDeath = 6;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("obstacle") && invincible == false)
        {
            die = true;
            deathSound.Play();
        }
    }

   
}
