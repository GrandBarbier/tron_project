﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class ScorePlayer : MonoBehaviour
{
        public float score;
        public TMP_Text scoreText;


        private void Update()
        {
                scoreText.text = score.ToString();
        }
}
