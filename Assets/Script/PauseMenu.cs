﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject menu;
    public bool isActive;
    public AudioSource music;
    // Start is called before the first frame update
    void Start()
    {
        menu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            isActive = !isActive;
            menu.SetActive(isActive);
        }

        if (menu.active)
        {
            Time.timeScale = 0;
            music.volume = 0.009f;
        }

        if (menu.active == false)
        {
            Time.timeScale = 1;
            music.volume = 0.05f;
        }
        
        
    }

    
    public void Menu()
    {
            SceneManager.LoadScene("MainMenu"); //démarrer le jeu (scene suivante)
    }
        
        
    
    public void Quit()
    {
        Application.Quit(); //quitter l'application
       
    }
}
