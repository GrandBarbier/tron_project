﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    public PlayerMovement[] playerMov;
    public List<GameObject> player = new List<GameObject>();
    public List<float> life = new List<float>();
    public List<bool> dead = new List<bool>();
    // Start is called before the first frame update
    void Start()
    {
        playerMov = FindObjectsOfType<PlayerMovement>();
        for (int i = 0; i < playerMov.Length; i++)
        {
            player.Add(playerMov[i].gameObject);
            life.Add(1);
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < player.Count; i++)
        {
            life[i] = player[i].GetComponent<Death>().life;
        }

        for (int i = 0; i < life.Count; i++)
        {
            if (i+1 <= life.Count)
            {
                if (life[i] <= 0)
                {
                    dead.Add(true);
                }
            }
        }


        if (dead.Count == life.Count -1)
        {
            SceneManager.LoadScene("EndGame");
        }
        
    }
}
