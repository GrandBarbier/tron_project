﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float maxSpeed;
    public float minSpeed;
    public float gravity;
    public float acceleration;
    public float deceleration;
    public float boostValue;
    public float turnSpeed;
    [Range(0, 1)] public float airControl;

    public Vector3 velocity;
    
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    
    public bool isGrounded;

    public float x;
    public float z;

    public Boost boost;
    
    // Start is called before the first frame update
    void Start()
    {
        controller = gameObject.GetComponent<CharacterController>();
        boost = gameObject.GetComponent<Boost>();
    }

    // Update is called once per frame
    void Update()
    {
        SetInputs();
        
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
        
        if (isGrounded)
        {
            
            if (x < -0.1 || x > 0.1)
            { 
                transform.Rotate(Vector3.up, x * Time.deltaTime * turnSpeed);
            }
            
            if (z > 0.1)
            {
                velocity.z = Mathf.Lerp(velocity.z, z * maxSpeed * boostValue, Time.deltaTime * acceleration);
            }
            else if (boost.keyUp && boost.empty == false)
            {
                velocity.z = Mathf.Lerp(velocity.z, minSpeed * boostValue * 2, Time.deltaTime * acceleration);
            }
            else
            {
                velocity.z = Mathf.Lerp(velocity.z, minSpeed, Time.deltaTime * acceleration);
            }
        }
        else
        {
            transform.Rotate(Vector3.up, x * Time.deltaTime * turnSpeed * airControl);
        }

        
        velocity.y += gravity * Time.deltaTime;

        Vector3 move = transform.forward * velocity.z + transform.up * velocity.y;
        controller.Move(move * Time.deltaTime);
    }


    void SetInputs()
    {
        if (gameObject.tag == "Player")
        {
            x = Input.GetAxis("Horizontal");
            z = Input.GetAxis("Vertical");
        }
        else if(gameObject.tag == "Player 2")
        {
            x = Input.GetAxis("Horizontal 2");
            z = Input.GetAxis("Vertical 2");
        }
        else if(gameObject.tag == "Player 3")
        {
            x = Input.GetAxis("Horizontal 3");
            z = Input.GetAxis("Vertical 3");
        }
        else if(gameObject.tag == "Player 4")
        {
            x = Input.GetAxis("Horizontal 4");
            z = Input.GetAxis("Vertical 4");
        }
        
        if (x < 0.1 && x > -0.1)
        {
            x = 0;
        }
    }
}
