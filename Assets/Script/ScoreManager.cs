﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreManager : MonoBehaviour
{
    public List<GameObject> player = new List<GameObject>();
    public PlayerMovement[] playerMov;
    public List<float> score = new List<float>();

    public float highestScore;

    // public float[] score;
    public string betterPlayer;

    // Start is called before the first frame update
    void Start()
    {
        playerMov = FindObjectsOfType<PlayerMovement>();
        for (int i = 0; i < playerMov.Length; i++)
        {
            player.Add(playerMov[i].gameObject);
            score.Add(0);
        }
    }

    // Update is called once per frame
    void Update()
    {



        for (int i = 0; i < player.Count; i++)
        {
            // Debug.Log(player[i].GetComponent<Score>().timerAddScore);
            score[i] = player[i].GetComponent<ScorePlayer>().score;
        }


        MaxValue();
        FindHigher();

    }


    public void MaxValue()
    {
        highestScore = score[0];

        for (int i = 0; i < score.Count; i++)
        {
            if (score[i] > highestScore)
            {
                highestScore = score[i];


            }
        }

        PlayerPrefs.SetFloat("score", highestScore);
    }

    public void FindHigher()
    {
        for (int i = 0; i < score.Count; i++)
        {
            if (score[i] == highestScore)
            {
                betterPlayer = player[i].tag;
                PlayerPrefs.SetString("string",betterPlayer);
            }
        }
    }
}
