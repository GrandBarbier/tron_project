﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikeVFX : MonoBehaviour
{
    public PlayerMovement playerMovement;
    public Death death;
    public GameObject bike;
    public float turnAngle;
    public float y;

    public float blinkRate;
    public float _actualBlink;
    
    public Vector3 actualAngle;
    
    public List<MeshRenderer> renderers = new List<MeshRenderer>();
    
    void Start()
    {
        playerMovement = gameObject.GetComponent<PlayerMovement>();
        death = gameObject.GetComponent<Death>();
        _actualBlink = blinkRate;

        for (int i = 0; i < 6; i++)
        {
            renderers.Add(bike.GetComponentsInChildren<MeshRenderer>()[i]);
        }
    }
    
    void Update()
    {
        actualAngle = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, -playerMovement.x * turnAngle);
        bike.transform.rotation = Quaternion.Euler(actualAngle);

        if (death.invincible)
        {
            if (_actualBlink > 0)
            {
                _actualBlink -= Time.deltaTime;
            }
            else if (_actualBlink <= 0)
            {
                for (int i = 0; i < renderers.Count; i++)
                {
                    renderers[i].enabled = !renderers[i].enabled;
                }
            }
        }
        else
        {
            for (int i = 0; i < renderers.Count; i++)
            {
                renderers[i].enabled = true;
            }
        }
    }
}
