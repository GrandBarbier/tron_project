﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;





public class Score : MonoBehaviour
{
    public Timer timeScript;
    
    public bool death;
    public ScorePlayer scoreP;
    
    public float timerValueMax;
    public float timerValue;
    public float timerScore;
    public float timerAddScore;
    
    public GameObject player;
   
  
    
    // Start is called before the first frame update
    void Start()
    {
        timerValueMax = timeScript.timerMax;
       
    }

    // Update is called once per frame
    void Update()
    {
        death = player.GetComponent<Death>().addScore;
        timerValue = timeScript.timer;
        if (death)
        {
            timerScore = timerValueMax - timerValue;
            timerValueMax = timerValue;
            timerScore = Mathf.FloorToInt(timerScore);

        }

        if (timerAddScore < timerScore)
        {
            timerAddScore = timerScore;
            scoreP.score = timerAddScore;
        }
        
        
       
     
      
       

    }
}
