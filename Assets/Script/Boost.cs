﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boost : MonoBehaviour
{
    public Slider jauge;

    public bool empty;

    public float valueBoost;

    public bool keyUp;

    [Range(1,2)]public float boostMutiply;
    public float maxSpeed;

    [Range(0, 1)] public float refillSpeed;
    public float useSpeed;

    public PlayerMovement playerMovement;


    private void Start()
    {
        maxSpeed = playerMovement.maxSpeed;
    }

    void Update()
    {
        SetInputs();
        
        if (empty== false && keyUp)
        {
            valueBoost -= Time.deltaTime * useSpeed;
            playerMovement.boostValue = boostMutiply;
            playerMovement.maxSpeed = maxSpeed * boostMutiply;
        }
        else
        {
            playerMovement.boostValue = 1f;
            playerMovement.maxSpeed = maxSpeed;
        }
        
        if (valueBoost <= 0)
        {
            valueBoost = 0;
            empty = true;
        }
        
        if (empty)
        {
            valueBoost += Time.deltaTime * refillSpeed;
        }
        
        if (valueBoost >= 1)
        {
            empty = false;
            valueBoost = 1;
        }

        jauge.value = valueBoost;
    }
    
    void SetInputs()
    {
        if (gameObject.tag == "Player")
        {
            keyUp = Input.GetButton("Boost");
            if (Input.GetButtonUp("Boost"))
            {
                empty = true;
            }
        }
        else if(gameObject.tag == "Player 2")
        {
            keyUp = Input.GetButton("Boost 2");
            if (Input.GetButtonUp("Boost 2"))
            {
                empty = true;
            }
        }
        else if(gameObject.tag == "Player 3")
        {
            keyUp = Input.GetButton("Boost 3");
            if (Input.GetButtonUp("Boost 3"))
            {
                empty = true;
            }
        }
        else if(gameObject.tag == "Player 4")
        {
            keyUp = Input.GetButton("Boost 4");
            if (Input.GetButtonUp("Boost 4"))
            {
                empty = true;
            }
        }
    }
}
