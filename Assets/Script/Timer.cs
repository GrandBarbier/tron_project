﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public float timerMax;
    public float timer;
    public int timerInt;
    public TMP_Text timerText;

    
    // Start is called before the first frame update
    void Start()
    {
        timer = timerMax;
    }

    // Update is called once per frame
    void Update()
    {

        timerInt = Mathf.FloorToInt(timer + 1);
        timerText.text = timerInt.ToString();
            
        if (timer >= 0)
        {
            timer -= Time.deltaTime;
        }

        if (timer <=0)
        {
            SceneManager.LoadScene("EndGame");
        }
    }
}
