﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailCollision : MonoBehaviour
{
    
    public Death death;
    public Transform origin;

    public float lifetime;

    public TrailRenderer trail;

    public float rate;

    public LayerMask playerLayer;
    
    public List<Vector3> trailPoints = new List<Vector3>();

    private float _actualrate;

    public ScorePlayer scoreP;

    public AudioSource deathSound;
    
    
    void Start()
    {
        _actualrate = rate;
        trail.time = lifetime;
        death = gameObject.GetComponent<Death>();
    }

    
    void Update()
    {
        _actualrate -= Time.deltaTime;
        if (_actualrate <= 0f)
        {
            StartCoroutine(AddPoint(lifetime));
            _actualrate = rate;
        }
        if (trailPoints.Count > 1)
        {
            for (int i = 0; i <= trailPoints.Count-2; i++)
            {
                //Debug.DrawLine(trailPoints[i],trailPoints[i+1]);
                RaycastHit hit;
                if(Physics.Linecast(trailPoints[i], trailPoints[i + 1],out hit, playerLayer))
                {
                    //Debug.Log(hit.transform.gameObject.name);
                    if (hit.collider.tag == gameObject.tag)
                    {
                        death.die = true;
                        deathSound.Play();
                    }
                    else
                    {
                        hit.collider.gameObject.GetComponent<Death>().die = true;
                        scoreP.score += 20;
                        deathSound.Play();
                    }
                }
            }
        }
    }
    
    IEnumerator AddPoint(float time)
    {
        trailPoints.Add(origin.position);
        yield return new WaitForSeconds(time);
        trailPoints.Remove(trailPoints[0]);
    }
}
