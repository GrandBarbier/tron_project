﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

public class EndMenu : MonoBehaviour
{
    public TMP_Text win;

    public float score;

    public string winner;
    // Start is called before the first frame update
    void Start()
    {
        score = PlayerPrefs.GetFloat("score");
        winner = PlayerPrefs.GetString("string");
        win.text =winner + " score " + score.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RestartButton()
    {
        PlayerPrefs.SetString("string", null);
        PlayerPrefs.SetFloat("score",0);
       
    }

    public void MainMenuButton()
    {
        PlayerPrefs.SetString("string", null);
        PlayerPrefs.SetFloat("score",0);
        SceneManager.LoadScene("MainMenu");

    }

    public void QuitButton()
    {
        PlayerPrefs.SetString("string", null);
        PlayerPrefs.SetFloat("score",0);
        Application.Quit();
    }
}
