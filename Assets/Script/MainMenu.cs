﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject BStart;
    public GameObject QuitB;
    public GameObject ControlB;
    public GameObject GText;
    public GameObject BBack;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void Player2()
    {
        SceneManager.LoadScene("2 Joueurs Test 1");
    }
    
    public void Player3()
    {
        SceneManager.LoadScene("3 Joueurs");
    }
    
    public void Player4()
    {
        SceneManager.LoadScene("4 Joueurs");
    }
    public void Quit()
    {
        Application.Quit(); //quitter l'application
        Debug.Log("Quit");
    }
    
    
}
